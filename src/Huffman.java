import java.util.*;

public class Huffman {
   // viide: http://rosettacode.org/wiki/Huffman_coding#Java
   // viide: https://stackoverflow.com/questions/40946487/huffman-code-explanation-java
   // viide: https://codereview.stackexchange.com/questions/147509/huffman-compressor-in-java
   private ArrayList<HuffmanByte> huffBytes = new ArrayList<HuffmanByte>();
   private HashMap<String, Byte> huffBitsToBytes = new HashMap<String, Byte>();

   private int hTreeEncodingLength = 0;

   private static final boolean DEBUG = false;

   /**
    * HuffmanByte - represents single byte from input, with the
    * frequency(count) of that byte in the input
    *
    * @author akaver
    *
    */
   static class HuffmanByte implements Comparable<HuffmanByte> {
      public byte b;
      public int freq;
      public String huffmanCode;

      public HuffmanByte(byte b, int freq) {
         this.b = b;
         this.freq = freq;
      }

      public int compareTo(HuffmanByte hItem) {
         if (freq < hItem.freq)
            return -1;
         if (freq > hItem.freq)
            return 1;
         if (b < hItem.b)
            return -1;
         if (b > hItem.b)
            return 1;

         throw new RuntimeException(
                 "Impossible to sort Huffman freq list, identical bytes found!!!");
      }

      public String toString() {
         if (b > 32)
            return String.format("'%1$c'-0x%1$02x; freq:%2$d; code:%3$s", b, freq,
                    huffmanCode);
         else
            return String.format(   "   -0x%1$02x; freq:%2$d; code:%3$s", b, freq,
                    huffmanCode);
      }
   }

   /**
    * HuffmanTree - binary tree, holding huffmanByte's
    */
   static class HuffmanTree implements Comparable<HuffmanTree> {
      public HuffmanTree left;
      public HuffmanTree right;
      public HuffmanByte code;
      public int sum;

      public HuffmanTree(HuffmanByte p) {
         left = null;
         right = null;
         code = p;
         sum = p.freq;
      }

      public HuffmanTree(HuffmanTree left, HuffmanTree right) {
         this.left = left;
         this.right = right;
         code = null;
         sum = left.sum + right.sum;
      }

      public int compareTo(HuffmanTree t) {
         return Integer.compare(sum, t.sum);
      }

      public int finalize(String s) {
         if (left == null && right == null) {
            if (s.equals(""))
               s = "0";
            code.huffmanCode = s;
            if (DEBUG)
               System.out.println(sum + " - " + code);
            return s.length() * code.freq;
         }
         if (DEBUG) {
            assert left != null;
            System.out.println(sum + " L:" + left.sum + " R:" + right.sum);
         }
         assert left != null;
         return left.finalize(s + "0") + right.finalize(s + "1");
      }
   }

   /**
    * Method to convert from frequency list to Huffman binary tree
    *
    * @param fList
    *            - ArrayList containing byte,freq pairs
    * @return binary huffman tree, converted from ArrayList
    */
   static HuffmanTree convertFreqListToHuffmanTree(Huffman fList) {
      ArrayList<HuffmanTree> bTree = new ArrayList<>();
      for (HuffmanByte huffByte : fList.huffBytes) {
         bTree.add(new HuffmanTree(huffByte));

      }
      while (bTree.size() > 1) {
         HuffmanTree t1 = bTree.get(0);
         bTree.remove(0);
         HuffmanTree t2 = bTree.get(0);
         bTree.remove(0);
         HuffmanTree t3 = new HuffmanTree(t1, t2);
         int index = Collections.binarySearch(bTree, t3);
         if (index < 0)
            index = -(index + 1);

         bTree.add(index, t3);
         if (DEBUG)
            System.out.println("bTree size: " + bTree.size()
                    + " element added at:" + index);
      }

      if (bTree.size() == 1)
         return bTree.get(0);

      return null;
   }

   /**
    * Constructor to build the Huffman code for a given bytearray.
    *
    * @param original
    *            source data
    */
   Huffman(byte[] original) {
      if (original == null || original.length == 0)
         throw new RuntimeException("Cant create huffman from empty list");

      int[] byteTable = new int[256];
      for (byte b : original) {
         byteTable[b]++;
      }

      for (int i = 0; i < byteTable.length; i++) {
         if (byteTable[i] != 0) {
            huffBytes.add(new HuffmanByte((byte) i, byteTable[i]));
         }
      }

      Collections.sort(huffBytes);
      if (DEBUG)
         System.out.println("Huffman freq list is: " + huffBytes);

      HuffmanTree hTree = convertFreqListToHuffmanTree(this);
      assert hTree != null;
      hTreeEncodingLength = hTree.finalize("");
      for (HuffmanByte singleByte : huffBytes) {
         huffBitsToBytes
                 .put(singleByte.huffmanCode, singleByte.b);
         if (DEBUG)
            System.out.print(singleByte.huffmanCode + "-" + singleByte.b
                    + " ");
      }
      if (DEBUG)
         System.out.println();

   }

   /**
    * Length of encoded data in bits.
    *
    * @return number of bits
    */
   public int bitLength() {
      return hTreeEncodingLength;
   }

   /**
    * Convert 8 bits in string format to byte
    *
    * @param bits
    *            String, up to 8 bits
    * @return byte, conversion from bits
    */
   private byte stringOf8BitsToByte(String bits) {
      return (byte) Integer.parseInt(bits, 2);
   }

   /**
    * Encoding the byte array using this prefixcode.
    *
    * @param origData
    *            original data
    * @return encoded data
    */

   public byte[] encode(byte[] origData) {

      String[] huffCodeLibrary = new String[256];

      for (HuffmanByte singleByte : huffBytes) {
         huffCodeLibrary[singleByte.b] = singleByte.huffmanCode;
      }

      ArrayList<Byte> res = new ArrayList<>();
      StringBuffer codeBuf = new StringBuffer();
      for (byte origDatum : origData) {
         codeBuf.append(huffCodeLibrary[origDatum]);
         while (codeBuf.length() >= 8) {
            if (DEBUG)
               System.out.print(codeBuf.substring(0, 8) + " ");
            res.add(stringOf8BitsToByte(codeBuf.substring(0, 8)));
            codeBuf.delete(0, 8);
         }

      }
      if (codeBuf.length() > 0) {
         while (codeBuf.length() < 8) {
            codeBuf.append("0");
         }
         res.add(stringOf8BitsToByte(codeBuf.substring(0, 8)));
         if (DEBUG)
            System.out.print(codeBuf);
      }
      if (DEBUG)
         System.out.println();

      byte[] finalRes = new byte[res.size()];
      Iterator<Byte> it2 = res.iterator();
      int i = 0;
      while (it2.hasNext()) {
         finalRes[i] = it2.next();
         i++;
      }
      return finalRes;
   }

   /**
    * Converts byte to String of 8 bits
    *
    * @param bits
    *            Byte to convert
    * @return String, 8 bits (always 8 characters)
    */
   private String byteToStringOf8Bits(byte bits) {
      StringBuilder res = new StringBuilder(Integer.toBinaryString(bits));
      while (res.length() < 8) {
         res.insert(0, "0");
      }
      return res.substring(res.length() - 8, res.length());
   }

   /**
    * Decoding the byte array using this prefixcode.
    *
    * @param encodedData
    *            encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode(byte[] encodedData) {
      ArrayList<Byte> res = new ArrayList<>();
      StringBuilder buf = new StringBuilder();
      StringBuilder decodeBuf = new StringBuilder();
      int bitsDecoded = 0;
      for (byte singleByte : encodedData) {
         if (DEBUG)
            System.out.print(byteToStringOf8Bits(singleByte) + " (");
         buf.append(byteToStringOf8Bits(singleByte));
         if (DEBUG)
            System.out.print(buf + ") ");

         do {
            decodeBuf.append(buf.charAt(decodeBuf.length()));
            if (DEBUG)
               System.out.print("|" + decodeBuf + "| ");
            if (huffBitsToBytes.containsKey(decodeBuf.toString())
                    && bitsDecoded < bitLength()) {

               res.add(huffBitsToBytes.get(decodeBuf.toString()));
               bitsDecoded = bitsDecoded + decodeBuf.length();
               if (DEBUG)
                  System.out.print("<"
                          + huffBitsToBytes.get(decodeBuf.toString())
                          + "," + bitsDecoded + "> ");
               buf.delete(0, decodeBuf.length());
               decodeBuf.setLength(0);
            }

         } while (buf.length() > decodeBuf.length()
                 && bitsDecoded < bitLength());

      }

      if (DEBUG)
         System.out.println();
      byte[] finalRes = new byte[res.size()];
      Iterator<Byte> it2 = res.iterator();
      int i = 0;
      while (it2.hasNext()) {
         finalRes[i] = it2.next();
         i++;
      }
      return finalRes;
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
      // TODO!!! Your tests here!
      String teks = "A";
      byte[] original = teks.getBytes();
      Huffman huff = new Huffman (original);
      byte[] koood = huff.encode (original);
      byte[] original2 = huff.decode (koood);
      // must be equal: original, original2
      System.out.println (Arrays.equals (original, original2));
      int lnngth = huff.bitLength();
      System.out.println ("Length of encoded data in bits: " + lnngth);
   }
}

